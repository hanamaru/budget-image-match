import re
import requests
import os
import json

from collections import OrderedDict
from multiprocessing.pool import ThreadPool


def fetch_url(entry):
    ret = ""
    path = 'data/' + str(entry["portal_id"]) + ".jpg"
    # print(path)
    if (not os.path.exists(path)):
        # or (os.path.getsize(file_path) == 0):
        if (entry['image_addr'].lower() == 'null'):
            print(entry)
            return None
        r = requests.get(entry["image_addr"], stream=True)
        if r.status_code == 200:
            with open(path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
        else:
            print(f"Error code: {r.status_code}")
            return None
        ret = f"Downloaded image id {entry['portal_id']} - Portal name: {entry['portal_name']}"
    return ret


def parse_coordinates(coor_url: str):
    """
    Get coordinates from string. 
    (Match the first two numbers as coordinates)
    :str coor_url: URL with coordinates.
    :return float tuple: coordinates. 
    """
    coordinates = re.findall(r"[-]?\d+(?:[\.]\d+)", coor_url)
    try:
        coordinates = ",".join([coordinates[0], coordinates[1]])
    except Exception as e:
        # print(e)
        return 
    # To check if valid coordinates. 
    try:
        latlng = re.search(r"^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$", coordinates, re.M|re.I)
        lat, lng = latlng.group().split(",")
    except Exception as e:
        return
    return (float(lat), float(lng))


def get_portals(portal_list_file="Portal_Export.csv"):
    """
    :param portal_list_file: file with portal names. 
    :return list portal_list: List of portals with lat/lng/image_addr/portal_name
    """
    portal_list = []
    count = 0
    try:
        # 卧槽了怎么reg又不工作T_T
        with open(portal_list_file, encoding="utf-8", newline='', errors="replace") as csvfile:
            data = csvfile.readlines()
        for line in data: 
            tmp = OrderedDict()
            coords = parse_coordinates(line)
            if (not coords):
                continue 
            # A line with valid portal information I guess
            tmp['lat'], tmp['lng'] = coords
            line = line.split(',')
            tmp['image_addr'] = line[-1].rstrip("\n")
            tmp['portal_name'] = line[0].strip('"')
            tmp['portal_id'] = count 
            portal_list.append(tmp)
            count = count + 1
    except FileNotFoundError:
        print(f"[ERROR] File not found: input/{portal_list_file}. "\
              f"Please place \"{portal_list_file}\" to proper location.")
        exit(1)
    return portal_list


def main():
    if not os.path.exists('data'):
        os.makedirs('data')

    portal_list = get_portals("portals.csv")
    with open("portal_info.json", 'w') as fout:
         json.dump(portal_list, fout)
    
    try:
        run = ThreadPool(12).imap_unordered(fetch_url, portal_list)
        for res in run:
            if res != "":
                print(res)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    main()