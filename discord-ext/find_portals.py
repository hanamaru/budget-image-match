from discord import Embed
from discord.ext import commands
import json
import random 

from not_really_database import BudgetDB
from image_match.signature_database_base import make_record

def load_portal_database():
    with open('portal_signature.json') as fin:
        obj = json.load(fin)
    sdb = BudgetDB(obj)
    return sdb

def draw_tool_coordiates(lat: float, lng: float, color="#a24ac3"):
    """
    A super random function for generating ingress draw tool markers. 
    :color: draw tool marker color. 
    :return: Example LOL
    {"type":"marker","latLng":{"lat":36.005057,"lng":-78.935627},"color":"#a24ac3"}
    """
    result = "{\"type\":\"marker\",\"latLng\":{\"lat\":%f,\"lng\":%f},\"color\":\"%s\"}"%(lat, lng, color)
    # res = {"type": "marker", "latLng": {"lat": lat, "lng": lng}, "color": color}
    return result

def print_portals(portal):
    # {'portal_name': 'Playground at Phillip St Park', 
    #'portal_id': 332, 
    # 'image_addr': 'http://lh3.googleusercontent.com/1M3lb3HdSC8imBiGEuJRqoptoh9ZlL4je3_k7G_EWVptLy1g6mVxmPfVhWayfs7cJO0rAOSlgVfUYz6bN2SnxKqL0JoqASO10at9SPM8', 
    # 'lat': 48.436165, 'lng': -89.259781, 
    hexadecimal = ''.join([random.choice('ABCDEF0123456789') for i in range(6)])
    lat, lng = portal['lat'], portal['lng']
    draw_tool_marker = draw_tool_coordiates(lat, lng, color=('#'+hexadecimal))
    embed = Embed(title=f'{portal["portal_name"]}', 
                  description=f'Distance: {portal["dist"]}',
                  colour=int(hexadecimal, 16))
    embed.set_image(url=portal['image_addr'])
    embed.add_field(name='Coordinates', 
                    value=f'{lat}, {lng}',
                    inline=False)
    embed.add_field(name='🚮 game link',
                    value=f'```https://intel.ingress.com/?pll={portal["lat"]},{portal["lng"]}```',
                    inline=False)
    embed.add_field(name="**IITC Drawtool markers**",
                    value=f"```[{draw_tool_marker}]```",
                    inline=False)
    return embed

sdb = load_portal_database()

class FindPortals(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if (len(message.attachments) > 0):
            attachment = message.attachments[0]
        else:
            return 
        img = await attachment.save('test.jpg')
        result = []
        result = sdb.budget_search_image(path=f'./test.jpg')
        if (len(result) == 0):
            await message.channel.send("I didn't find anything!")
            return
        for portal in result:
            await message.channel.send(embed=print_portals(portal))