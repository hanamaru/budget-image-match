# budget image match

Based on image match (https://github.com/ProvenanceLabs/image-match) for matching images in IFS portal hunt. 

## Getting started

- Get portal list with IITC script `IITC plugin: Portal Multi Export`, export as csv. 
(Download url: https://iitc.aradiv.de/plugin/37/multi_export.user.js )

- Download portal images with `data_download.py`

```
python data_download.py
```
It's a budget script and I didnt expect anyone besides me to use it, so once it stopped downloading data for some reasons, you have to manually restart ;w;

- Generate image signatures

```
python generate_signature.py
```

## Match images with discord bot extension

I have no idea how to search for portals with some screenshot so I wrapped it into a discord bot. 

## How to run

- Copy `portal_signature.json` and `portal_info.json` to `discord-ext/` folder.
- Copy `not_really_database.py` to `discord-ext/` folder;
- Edit main.py.
  `bot.run('TOEKN')` Token to your bot token
- `python main.py`