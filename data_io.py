import json

def open_file(filename):
    """
    Open the json file. 
    """
    try:
        with open(filename, 'r') as fin:
            obj = (json.load(fin))
    except Exception as e:
        print(e)
    return obj

def write_file(filename, obj):
    """
    Write the database to JSON file. 
    :param filename: JSON file name. 
    :param dict obj: Database object.
    """
    pass