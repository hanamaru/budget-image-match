import discord
from discord.ext import commands

from find_portals import FindPortals

bot = commands.Bot(command_prefix='?', description='Hi')

@bot.event
async def on_ready():
    print('Logged in as:')
    print(bot.user.name)
    print(bot.user.id)
    print('-----')
    bot.add_cog(FindPortals(bot))

if (__name__ == '__main__'):
    bot.run('TOKEN')