from not_really_database import BudgetDB
from data_io import *
from image_match.signature_database_base import make_record


def open_signature_files():
    try:
        sdb = open_file(filename='portal_signature.json')
    except Exception as e:
        print(e)
        sdb = []
    finally:
        signature_db = BudgetDB(sdb)
    return signature_db


def generate_portal_metadata(portals_to_add, sdb):
    """
    :param dict portals: portal data (without signature lol)
    :param dict sdb: portal signature database. 
    """
    for portal in portals_to_add:
        if (portal['portal_id'] % 50 == 0):
            print(f"Processing Portal: {portal['portal_name']} ({portal['portal_id']})")
        image = f'./data/{portal["portal_id"]}.jpg'
        try:
            sdb.add_image(path=portal['image_addr'],
                      img=image, 
                      metadata=portal)
        except Exception as e:
            print(e)
            print(f"Something went wrong! \nCheck portal {portal['portal_name']}({portal['portal_id']})")
    with open("portal_signature.json", 'w') as fout:
        json.dump(sdb.db, fout, default=str)


def get_features():
    portals = open_file(filename='portal_info.json')
    sdb = open_signature_files()
    generate_portal_metadata(portals, sdb)


def main():
    sdb = open_signature_files()
    result = sdb.budget_search_image(path='./what.jpg')
    print(result)


if (__name__ == '__main__'):
    get_features()