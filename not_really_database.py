from image_match.signature_database_base import SignatureDatabaseBase, make_record
from image_match.goldberg import ImageSignature as gis
import numpy as np


class BudgetDB(SignatureDatabaseBase):
    """
    Decided to save the records as list and then lol
    I know it's very stupid but I have no idea how to use sqlite D:
    """

    def __init__(self, db, *args, **kwargs):
        """
        :param db: A list with portal names
        As the class name indicates it's not really a database. 
        database structure(planned):
        {
            portal_name: "Original Blue Devil Logo",
            portal_id: 1, # Generated automatically from download_data.py.
            image_addr: "", (which can be imported from IITC),
            lat: ,
            lng: ,
            rec: {
                something_from_make_record
            }

        }
        """
        self.db = db
        super(BudgetDB, self).__init__(*args, **kwargs)
    
    def search_single_record(self, rec, pre_filter=None):
        """
        Search for records in the database 
        """
        result = []
        signature_for_match = np.array(rec.pop('signature'))
        path = rec.pop('path')
        simple_words = set(rec.values())
        for portal in self.db:
            portal_rec = portal['rec'].copy()
            portal_signature = np.array(portal_rec.pop('signature'))
            portal_simple_words = set(portal_rec.values())
            if len(simple_words.intersection(portal_simple_words)) > 0:
                # and (normalized_distance(signature_for_match, portal_signature) < 0.5)):
                dist = gis.normalized_distance(signature_for_match, portal_signature)
                portal['dist'] = dist
                if (dist < 0.4):
                    # Image distance ;w; 
                    # I always set it to 0.5 for finding portals in IFS.
                    result.append(portal)
        result = sorted(result, key=lambda p: p['dist'])
        return result

        # return super().search_single_record(rec, pre_filter=pre_filter)
    
    def insert_single_record(self, rec, refresh_after=False):
        """
        :param rec: rec from signature_database_base.make_record()
        :param refresh_after: Not sure what it's but it's required ;w;
        """
        try:
            metadata = rec.pop('metadata')
        except Exception:
            pass
        self.db.append({
            'portal_name': metadata['portal_name'],
            'portal_id': metadata['portal_id'],
            'image_addr': metadata['image_addr'],
            'lat': metadata['lat'],
            'lng': metadata['lng'],
            'rec': rec
        })
        # return super().insert_single_record(rec)
    
    def budget_search_image(self, path, bytestream=False):
        """
        Actually it's from search_image, but I have no idea why it doesn't work.
        So as its name says it's a budget one...
        :param path: path or image data. If bytestream=False, then path is assumed to be
                    a URL or filesystem path. Otherwise, it's assumed to be raw image data
        bytestream (Optional[boolean]): will the image be passed as raw bytes?
                That is, is the 'path_or_image' argument an in-memory image?
                (default False)
        """
        rec = make_record(path, self.gis, self.k, self.N)
        l = self.search_single_record(rec)
        return(l)